/** CONTROL PANEL BUTTONS */
$("button#signOut").click(function () {
    var url = 'lib/admin.php';

    var posting = $.post( url );
    posting.done(function() {
        location.reload();
    });
});

$("button#recovery").click(function () {
    var url = 'lib/recovery.php';

    var posting = $.post( url );
    posting.done(function() {
        location.reload();
    });
});

$("button#reset").click(function () {
    var proceed2Exit = confirm('리셋 된 데이터는 되돌릴 수 없습니다. 계속하시겠습니까?');
    if(proceed2Exit) {
        var posting = $.post('lib/postLibrary.php', {"function": "reset"});
        posting.done(function() {
            location.reload();
        });
    }
});

/** LIST BUTTONS */
$("button.delete").click(function () {
    var id = $(this).attr("id");
    var proceed2Exit = confirm('해당 글을 완전히 삭제합니다. 계속하시겠습니까?');
    if(proceed2Exit) {
        var posting = $.post('lib/postLibrary.php', { "id": id, "function": "delete"});
        posting.done(function() {
            location.reload();
        });
    }
});

$("button.hide").click(function () {
    var id = $(this).attr("id");
    var posting = $.post('lib/postLibrary.php', { "id": id, "function": "hide"});
    posting.done(function() {
        location.reload();
    });
});

$("button.restore").click(function () {
    var id = $(this).attr("id");
    var posting = $.post('lib/postLibrary.php', { "id": id, "function": "restore"});
    posting.done(function() {
        location.reload();
    });
});

$("button.pin").click(function () {
    var id = $(this).attr("id");
    var posting = $.post('lib/postLibrary.php', { "id": id, "function": "pin"});
    posting.done(function() {
        location.reload();
    });
});

$("button.like").click(function () {
    var id = $(this).attr("id");
    var liked = $(this).children("span").html();
    var posting = $.post('lib/postLibrary.php', { "id": id, "function": "like", "liked": liked});

    posting.done(function() {
        location.reload();
    });
});

$(".pinned button.pin").click(function () {
    var id = $(this).attr("id");
    var posting = $.post('lib/postLibrary.php', { "id": id, "function": "unpin"});
    posting.done(function() {
        location.reload();
    });
});

$(".ip").click(function () {
    var ip = $(this).html();
    // alert(ip);
});

$(".session").click(function () {
    var session = $(this).html();
    // alert(session);
});
