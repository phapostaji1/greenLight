if ($("#msg").length) {
    $("#msg").slideDown("slow");

    setTimeout(function () {
        $("#msg").slideUp("slow");
    }, 10000)
}

$("#closeMsg").click(function () {
    $("#msg").slideUp("slow");
});
$('#adminSignIn button').click(function() {
    var inputBox = $("#adminSignIn div");
    var divContainer = $("#adminSignIn");

    divContainer.animate({
        width: parseInt(divContainer.css('width')) == 28? 200: 28
    }).toggleClass("adminSignOpen");

    inputBox.toggle();

});

$(".pinned .icon i").attr('class', 'fa fa-thumb-tack');
$(".pinned div.comment").attr('class', 'post');
$(".commentWriterToggle").click(function() {
    $(this).next().slideToggle();
});

$("#config").click(function() {
    $(this).next().slideToggle();
});

$(".trafficLight+label").click(function () {
    var icon = $(this).children("i");

    if (icon.attr("class") == "fa fa-male") {
        icon.attr('class', 'fa fa-child')
    } else {
        icon.attr('class', 'fa fa-male');
    }
});

$(".timePosted span").click(function () {
    var ago = $(this).html();
    var time = $(this).attr("title");
    $(this).html(time);
    $(this).attr("title", ago);
});
