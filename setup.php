<?php
session_start();

include_once "lib/misc.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $commName = $_POST["name"];
    $adminPassword = password_hash($_POST["password"], PASSWORD_DEFAULT);
    $host = $_POST["dbHost"];
    $username = $_POST["dbUserName"];
    $password = $_POST["dbPassword"];
    $dbname = $_POST["dbName"];

    /** DB TABLE INFORMATION */
    $tableName = $commName."GreenLight";

    /** SQL SCRIPTS */
    $sql = <<<EOT
CREATE TABLE $tableName(
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pid INT(11) DEFAULT 0,
    content TEXT,
    timePosted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    timeUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    liked INT(11) DEFAULT 0,
    clientIP VARCHAR(45),
    forwardedIP VARCHAR(45),
    remoteIP VARCHAR(45),
    sessionID VARCHAR(255),
    isGreen TINYINT(1) DEFAULT 0,
    isPinned TINYINT(1) DEFAULT 0,
    isAdmin TINYINT(1) DEFAULT 0,
    isVisible TINYINT(1) DEFAULT 1
)
EOT;


    /** CREATE TABLES */
    $mysqli = new mysqli($host, $username, $password, $dbname);

    if ($mysqli->connect_errno) {
        printf("Connect failed: %s\n", $mysqli->connect_error);
        exit();
    } else {
        /** CREATE TABLES */
        if(!$mysqli->query($sql)) {
            echo "Error: DB TABLE is not created.";
            exit();
        }
    }

    /** CLOSE CONNECTION */
    $mysqli->close();

    /** WRITE TO config.json FILE */
    $config = array(
        "tableName"=>$tableName,
        "dbHost"=>$host,
        "dbUser"=>$username,
        "dbName"=>$dbname,
        "dbPassword"=>$password,
        "adminPassword"=>$adminPassword,
        "title"=>":::그린 라이트 보드:::",
        "home"=>"그린 라이트 보드",
        "numPost"=>9,
        "numPage"=>3,
        "postTimeLimit"=>30
    );

    $fp = fopen('data/config.json', 'w+');

    if (!$fp) {
        $sql = "DROP TABLE $tableName;";
        $mysqli->query($sql);

        echo "Error: config file was not created.";
        exit();
    }

    fwrite($fp, json_encode($config));
    fclose($fp);

    $_SESSION["msg"] = "그린라이트 게시판이 성공적으로 생성되었습니다. 보안을 위해 setup.php 파일을 반드시 삭제해주세요.";

    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>:::그린라이트 보드 설치:::</title>
    <style>

    </style>
</head>
<body>
<main>
    <h1>그린라이트 보드 설치 페이지</h1>
    <p>그린라이트 보드 설치를 위해 다음 폼을 작성하세요. 설치가 끝난 후에는 보안을 위해 FTP에서 해당 폴더 내의 lib/setup.php 파일을 삭제하십시오.</p>
    <form action="<?php textify($_SERVER['PHP_SELF']) ?>" method="post">
        <h3>그린라이트 보드 관리자 정보</h3>
        <div class="inputHolder"><input type="text" name="name" placeholder="커뮤 아이디" pattern="[A-Za-z]{3,}" title="3자 이상의 영문으로 작성하세요." required></div>
        <div class="inputHolder"><input type="password" name="password" placeholder="관리자 비밀번호" required></div>

        <h3>DB 정보</h3>
        <div class="inputHolder"><input type="text" name="dbHost" placeholder="DB 호스트 네임" required></div>
        <div class="inputHolder"><input type="text" name="dbUserName" placeholder="DB 유저 네임" required></div>
        <div class="inputHolder"><input type="text" name="dbName" placeholder="DB 네임" required></div>
        <div class="inputHolder"><input type="password" name="dbPassword" placeholder="DB 비밀번호" required></div>
        <div class="inputHolder"><input type="submit" name="submit" style="visibility: hidden;" required></div>
    </form>
</main>
</body>
</html>
