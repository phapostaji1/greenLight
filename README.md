# 그린 라이트 보드
익명 관캐상담 게시판 그린 라이트 보드입니다. 이 사이트에서 다운받아서 자유롭게 사용/수정/배포 가능합니다.

## System Requirement
이 소프트웨어는 php version 6.4 이상의 환경에서만 테스트 되었습니다.

## How to Get Started
### 준비물
게시판을 업로드 할 서버와 Mysql DB가 필요합니다.

### 설치
1. 설치를 희망하는 서버의 폴더안에 이 소프트웨어를 업로드 하세요.
2. ```setup.php``` 에 들어가서 요구되는 정보를 입력하세요.
3. 설치 성공 메시지가 뜨면 ```setup.php``` 파일을 삭제하십시오.

## Copyright
Copyright (c) 2018 Kiwoong

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.