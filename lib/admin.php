<?php
session_start();

include_once "misc.php";
include_once "DBCxn.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(isset($_SESSION['isAdmin'])) {
        unset($_SESSION['isAdmin']);
        unset($_SESSION['recoveryMode']);
    } else {

        /** GET CONFIG */
        $config = getConfig();
        $adminPass = $config["adminPassword"];

        if(password_verify(textify($_POST['adminPass']),$adminPass)) {
            $_SESSION["isAdmin"] = 1;
            $_SESSION["msg"] = "성공적으로 로그인되셨습니다.";
        } else {
            $_SESSION["msg"] = "비밀번호 인증 실패";
        }
    }
}

header('Location: ../index.php');
exit;
