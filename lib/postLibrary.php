<?php
session_start();

include_once "misc.php";
include_once "DBCxn.php";

function updatePost ($id, $attr, $val) {
    $mysqli = DBCxn::get();
    $config = getConfig();
    $table = $config["tableName"];

    $query = "UPDATE $table SET $attr='$val' WHERE id='$id'";
    $mysqli->query($query);
}

function deletePost ($id) {
    $mysqli = DBCxn::get();
    $config = getConfig();
    $table = $config["tableName"];

    $query = "DELETE FROM $table WHERE id='$id'";
    $mysqli->query($query);
}

function deleteAll () {
    $mysqli = DBCxn::get();
    $config = getConfig();
    $table = $config["tableName"];

    $query = "TRUNCATE $table";
    $mysqli->query($query);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST["id"];

    switch ($_POST["function"]) {
        case "reset":
            deleteAll();
            break;
        case "hide":
            updatePost($id, "isVisible", 0);
            updatePost($id, "isPinned", 0);
            break;
        case "delete":
            deletePost($id);
            break;
        case "restore":
            updatePost($id, "isVisible", 1);
            break;
        case "pin":
            updatePost($id, "isPinned", 1);
            updatePost($id, "timeUpdated", date("Y-m-d H:i:s"));
            $_SESSION["msg"] = date("Y-m-d H:i:s");
            break;
        case "unpin":
            updatePost($id, "isPinned", 0);
            break;
        case "like":
            $currentLiked = $_POST["liked"];
            updatePost($id, "liked", ++$currentLiked);
            break;
    }
}