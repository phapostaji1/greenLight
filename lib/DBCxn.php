<?php
include_once "misc.php";

class DBCxn {
    private static $link;

    final private function __construct() {}
    final private function __clone() {}

    public static function get() {
        // connect if not already connected
        if(is_null(self::$link)) {
            $db = getConfig();
            self::$link = new mysqli($db['dbHost'], $db['dbUser'], $db['dbPassword'], $db['dbName']);
        }
        return self::$link;
    }
}
