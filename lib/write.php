<?php
session_start();

include_once "misc.php";
include_once "DBCxn.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    /** GET CONFIG */
    $config = getConfig();
    $table = $config["tableName"];
    $timeLimit = $config["postTimeLimit"];

    /** GET POST DATA */
    $pid = isset($_POST["pid"])? $_POST["pid"]: 0;
    $postContent = textify($_POST["postContent"]);
    $timePosted = date("Y-m-d H:i:s");
    $clientIP = $_SERVER['HTTP_CLIENT_IP']? $_SERVER['HTTP_CLIENT_IP']: 0;
    $forwardedIP = $_SERVER['HTTP_X_FORWARDED_FOR']? $_SERVER['HTTP_X_FORWARDED_FOR']: 0;
    $remoteIP = $_SERVER['REMOTE_ADDR']? $_SERVER['REMOTE_ADDR']: 0;
    $sessionID = $_POST["sessionID"];
    $isPinned = $_POST["isPinned"]? 1: 0;
    $isGreen = isset($_POST["pid"]) && $_POST["isGreen"]? 1: 0;
    $isAdmin = $_SESSION["isAdmin"]? 1: 0;

    /** GET POST TIME LIMIT */
    if(isset($_SESSION["timeLimit"]) && (strtotime($_SESSION["timeLimit"]) > time())) {
        $_SESSION["msg"] = "이전 글을 작성한 지 ".$config["postTimeLimit"]."초가 지나야 새로운 글을 작성할 수 있습니다.";
        $_SESSION["content"] = $_POST["postContent"];

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }

    if(empty($postContent)) {
        $_SESSION["msg"] = "내용을 입력하세요.";

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }

    /** DB */
    $mysqli = DBCxn::get();

    $query = <<<EOT
INSERT INTO $table(
    pid,
    content,
    timePosted,
    clientIP,
    forwardedIP,
    remoteIP,
    sessionID,
    isGreen,
    isPinned,
    isAdmin
)
VALUES(
    $pid,
    "$postContent",
    "$timePosted",
    "$clientIP",
    "$forwardedIP",
    "$remoteIP",
    "$sessionID",
    $isGreen,
    $isPinned,
    $isAdmin
);
EOT;

    $mysqli->query($query);

    /** SET TIME LIMIT */
    $_SESSION["timeLimit"] = date("m/d/Y h:i:s a", time() + $timeLimit);
}

header('Location: ' . $_SERVER['HTTP_REFERER']);
exit;
?>
