<?php
date_default_timezone_set("Asia/Seoul");
session_start();

/** READ JSON FILE */
function readJson($url) {
    /* Load db information */
    $contents = file_get_contents($url);
    $results = json_decode($contents, true);

    if (json_last_error() === JSON_ERROR_NONE) {
        if(is_null($results)){
            echo "Error triggered while decoding Json file";
        } else {
            return $results;
        }
    } else {
        echo "problem reading ".$url." file: ";
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                echo 'No errors';
                break;
            case JSON_ERROR_DEPTH:
                echo 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                echo 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                echo 'Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                echo 'Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                echo 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                echo 'Unknown error';
                break;
        }
    }
    return -1;
}

function getConfig() {
    return readJson(dirname(__DIR__)."/data/config.json");
}

/** GET IP ADDRESS
 * @param $clientIP
 * @param $forwardedIP
 * @param $remoteIP
 * @return string
 */
function getIP($clientIP, $forwardedIP, $remoteIP) {
    if ($clientIP === "0") {
        if($forwardedIP === "0") {
            if($remoteIP === "0") {
                return "UNKNOWN";
            }
            return $remoteIP;
        }
        return $forwardedIP;
    }
    return $clientIP;
}

function pluralize( $count, $text )
{
    return $count.(($count == 1)? (" $text"): (" ${text}s"));
}

function ago( $datetime )
{
    $interval = date_create('now')->diff( $datetime );
    $suffix = ( $interval->invert ? ' ago' : '' );

    if ( $v = $interval->y >= 1 ) return pluralize( $interval->y, 'year' ) . $suffix;
    if ( $v = $interval->m >= 1 ) return pluralize( $interval->m, 'month' ) . $suffix;
    if ( $v = $interval->d >= 1 ) return pluralize( $interval->d, 'day' ) . $suffix;
    if ( $v = $interval->h >= 1 ) return pluralize( $interval->h, 'hour' ) . $suffix;
    if ( $v = $interval->i >= 1 ) return pluralize( $interval->i, 'minute' ) . $suffix;
    return pluralize($interval->s, 'second').$suffix;
}

function parseRow ($row)
{
    $pid = $row['pid'];
    $content = nl2br($row['content']);
    $isAdminRow = $row['isAdmin'];
    $isGreenRow = $row['isGreen'];
    $timePosted = $row['timePosted'];
    $timeAgo = ago(new DateTime($timePosted));

    $postFormatter = "%s%s</div>%s";
    $timePosted = "<div class='timePosted'><span title='$timePosted'>$timeAgo</span></div>";

    if ($isAdminRow) {
        $content = "<b>$content</b>";
    }

    if ($pid > 0) {
        /** Comment starts with green light */
        if ($isGreenRow) $icon = "<span class='icon green'><i class='fa fa-child' aria-hidden='true'></i></span><div class='comment'>";
        else $icon = "<span class='icon red'><i class='fa fa-male' aria-hidden='true'></i></span><div class='comment'>";


    } else {
        $icon = "<span class='icon'><i class='fa fa-question-circle' aria-hidden='true'></i></span><div class='post'>";
    }

    return sprintf($postFormatter, $icon, $content, $timePosted);
}

/**
 * @param $id
 * @param $liked
 * @return string
 */
function getPublicFunctions ($id, $liked) {
    $likeBTN = "<button id='%d' class='like%s'><i class='fa fa-heart' aria-hidden='true'></i><span>%s</span></button>";

    if ($liked >= 1000) {
        $functions = sprintf($likeBTN, $id, " red", floor($liked / 1000)."K");
    } elseif ($liked > 0) {
        $functions = sprintf($likeBTN, $id, " red", $liked);
    } else {
        $functions = sprintf($likeBTN, $id, "", $liked);
    }

    return $functions;
}

/**
 * @param $id
 * @param $ipRow
 * @param $sessionRow
 * @return string
 */
function getAdminFunctions ($id, $ipRow, $sessionRow) {
    $pinBTN = "<button id='%d' class='pin'><i class='fa fa-thumb-tack' aria-hidden='true'></i></button>";
    $deleteBTN = "<button id='%d' class='hide'><i class='fa fa-trash' aria-hidden='true'></i></button>";
    $ipBTN = "<span class='ip'>%s</span>";
    $sessionBTN = "<span class='session'>%s</span>";

    $functions = sprintf($pinBTN, $id);
    $functions .= sprintf($deleteBTN, $id);
    $functions .= sprintf("<div class='info'>ip:&nbsp;".$ipBTN."&nbsp;&nbsp;session id:&nbsp;".$sessionBTN."</div>", $ipRow, $sessionRow);

    return $functions;
}

/**
 * @param $id
 * @param $ipRow
 * @param $sessionRow
 * @return string
 */
function getRecoveryFunctions ($id, $ipRow, $sessionRow) {
    $restoreBTN = "<button id='%d' class='restore'><i class='fa fa-repeat' aria-hidden='true'></i></button>";
    $deleteBTN = "<button id='%d' class='delete'><i class='fa fa-eraser' aria-hidden='true'></i></button>";
    $ipBTN = "<span class='ip'>%s</span>";
    $sessionBTN = "<span class='session'>%s</span>";

    $functions = sprintf($restoreBTN, $id);
    $functions .= sprintf($deleteBTN, $id);
    $functions .= sprintf("<div class='info'>ip:&nbsp;".$ipBTN."&nbsp;&nbsp;session id:&nbsp;".$sessionBTN."</div>", $ipRow, $sessionRow);

    return $functions;
}

/**
 * @param $id
 * @return string
 */
function getCommentBTN ($id) {
    $commentBTN = "<div id='$id' class='commentWriterToggle'><i class='fa fa-chevron-down' aria-hidden='true'></i>&nbsp;댓글달기</div>";
    $commentBTN .= "<div class='commentWriter' id='$id'><form action='lib/write.php' method='post'>";
    $commentBTN .= "<textarea name='postContent' required></textarea><input type='submit' value='Post'><input type='hidden' name='pid' value='$id'><input type='hidden' name='sessionID' value='".session_id()."'>";
    $commentBTN .= "<input class='trafficLight' id='trafficLight$id' type='checkbox' name='isGreen' value='1'><label for='trafficLight$id'><i class='fa fa-male' aria-hidden='true'></i></label></form></div>";

    return $commentBTN;
}

/** SECURITY */
function textify($data) {
//    $data = nl2br($data);
    $data = preg_replace('/[ ]{2,}|[\t]/', ' ', trim($data)); //trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data, ENT_QUOTES, 'ISO-8859-1');
    return $data;
}

//include_once("DBCxn.php");
//$mysql = DBCxn::get();
?>