<?php
session_start();

include_once "misc.php";
include_once "DBCxn.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    /** GET CONFIG */
    $config = getConfig();
    $newPassword = $_POST["newPassword"];

    if ($newPassword) {
        $newPassword = password_hash(textify($newPassword), PASSWORD_DEFAULT);
        $config['adminPassword'] = $newPassword;
    }

    $config["title"] = textify($_POST["newTitle"]);
    $config["home"] = textify($_POST["newHome"]);
    $config["numPost"] = $_POST["newNumPost"];
    $config["numPage"] = $_POST["newNumPage"];
    $config["postTimeLimit"] = $_POST["newTimeLimit"];

    $fp = fopen(dirname(__DIR__).'/data/config.json', 'w+');
    fwrite($fp, json_encode($config));
    fclose($fp);

    $_SESSION["msg"] = "보드 정보가 업데이트 되었습니다.";
}

header('Location: ../index.php');
exit;
