<?php
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_SESSION["recoveryMode"]) && $_SESSION["recoveryMode"] == 1) {
        unset($_SESSION["recoveryMode"]);
    } else {
        $_SESSION["recoveryMode"] = 1;
    }
}