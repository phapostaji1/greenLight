<?php
session_start();
//phpinfo();
include_once "lib/misc.php";
include_once "lib/DBCxn.php";

/** LOAD CONFIG */
$config = getConfig();
$table = $config["tableName"];
$numPost = $config["numPost"];
$numPage = $config["numPage"];
$title = $config["title"];
$postTimeLimit = $config["postTimeLimit"];
$home = $config["home"];

/** GET USER INFO */
$clientIP = $_SERVER['HTTP_CLIENT_IP'];
$forwardedIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
$remoteIP = $_SERVER['REMOTE_ADDR'];
$sessionID = session_id();
$isAdmin = isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1? 1: 0;
$isRecoveryMode = isset($_SESSION['recoveryMode']) && $_SESSION['recoveryMode'] == 1? 1: 0;

/** GET PAGE INFORMATION */
$page = $_GET["page"]? $_GET["page"]: 1;

$startFrom = ($page - 1) * $numPost;

/** DEFINE QUERIES */
if ($isRecoveryMode) {
    /* In recovery mode you get to see all the comments in the table */
    $queryPinned = "SELECT * FROM $table WHERE isPinned=1 ORDER BY timeUpdated ASC";
    $queryPost = "SELECT * FROM $table WHERE pid=0 ORDER BY id DESC LIMIT $startFrom, $numPost";
    $queryComment = "SELECT * FROM $table WHERE pid=%d ORDER BY id ASC";
    $queryPage = "SELECT COUNT(id) AS num FROM $table WHERE pid=0";
} else {
    /* If not recovery mode, display only visible posts */
    $queryPinned = "SELECT * FROM $table WHERE isVisible=1 AND isPinned=1 ORDER BY timeUpdated ASC";
    $queryPost = "SELECT * FROM $table WHERE pid=0 AND isVisible=1 ORDER BY id DESC LIMIT $startFrom, $numPost";
    $queryComment = "SELECT * FROM $table WHERE pid=%d AND isVisible=1 ORDER BY id";
    $queryPage = "SELECT COUNT(id) AS num FROM $table WHERE pid=0 AND isVisible=1";
}

/** SQL CONNECT */
$mysqli = DBCxn::get();

/** HTML STARTS HERE */
include_once "page/header.html";

/** CONTROL PANEL */
if ($isRecoveryMode) {
    include_once "page/recovery.html";
} elseif ($isAdmin) {
    include_once "page/admin.html";
    include_once "page/write.html";
} else {
    include_once "page/write.html";
    include_once "page/signIn.html";
}

/** PRINT PINNED POSTS */
$resultPinned = $mysqli->query($queryPinned);

if ($resultPinned->num_rows > 0) {
    echo "<ul class='posts pinned'>";

    while($rowPost = $resultPinned->fetch_assoc()) {
        /** PINNED POSTS */
        $id = $rowPost['id'];
        $content = $rowPost['content'];
        $liked = $rowPost['liked'];
        $ipPost = getIP($rowPost['clientIP'], $rowPost['forwardedIP'], $rowPost['remoteIP']);
        $isVisible = $rowPost['isVisible'];
        $sessionPost = $rowPost['sessionID'];

        if ($isRecoveryMode) {
            $functions = getRecoveryFunctions($id, $ipPost, $sessionPost);
        } elseif ($isAdmin) {
            $functions = getAdminFunctions($id, $ipPost, $sessionPost);
        } else {
            $functions = getPublicFunctions($id, $liked);
        }

        if ($isVisible) {
            echo "<li id='$id'>".parseRow($rowPost).$functions."</li>";
        } else {
            echo "<li id='$id' class='hidden'>".parseRow($rowPost).$functions."</li>";
        }
    }

    echo "</ul>";
}

/** PRINT REGULAR POSTS */
$resultPost = $mysqli->query($queryPost);

if ($resultPost->num_rows > 0) {
    echo "<ul class='posts'>";

    while($rowPost = $resultPost->fetch_assoc()) {
        /** POSTS */
        $id = $rowPost['id'];
        $content = $rowPost['content'];
        $liked = $rowPost['liked'];
        $isVisible = $rowPost['isVisible'];
        $ipPost = getIP($rowPost['clientIP'], $rowPost['forwardedIP'], $rowPost['remoteIP']);
        $sessionPost = $rowPost['sessionID'];

        if ($isRecoveryMode) {
            $functions = getRecoveryFunctions($id, $ipPost, $sessionPost);
        } elseif ($isAdmin) {
            $functions = getAdminFunctions($id, $ipPost, $sessionPost);
            $functions .= getCommentBTN($id);
        } else {
            $functions = getPublicFunctions($id, $liked);
            $functions .= getCommentBTN($id);
        }

        if ($isVisible) {
            echo "<li>".parseRow($rowPost).$functions."</li>";
        } else {
            echo "<li class='hidden'>".parseRow($rowPost).$functions."</li>";
        }

        $resultComment = $mysqli->query(sprintf($queryComment, $id));

        if ($resultComment->num_rows > 0) {
            /** COMMENTS */
            echo "<ul class='comments'>";
            while($rowComment = $resultComment->fetch_assoc()) {
                $id = $rowComment['id'];
                $content = $rowComment['content'];
                $liked = $rowComment['liked'];
                $isVisible = $rowComment['isVisible'];
                $ipComment = getIP($rowComment['clientIP'], $rowComment['forwardedIP'], $rowComment['remoteIP']);
                $sessionComment = $rowComment['sessionID'];

                if ($isRecoveryMode) {
                    $functions = getRecoveryFunctions($id, $ipComment, $sessionComment);
                } elseif ($isAdmin) {
                    $functions = getAdminFunctions($id, $ipComment, $sessionComment);
                } else {
                    $functions = getPublicFunctions($id, $liked);
                }

                if ($isVisible) {
                    echo "<li>".parseRow($rowComment).$functions."</li>";
                } else {
                    echo "<li class='hidden'>".parseRow($rowComment).$functions."</li>";
                }
            }

            echo "</ul>";
        }
    }

    echo "</ul>";
} else {
    echo "<div id='emptyPosts'>아직 등록된 게시글이 없습니다.</div>";
}

echo "</main><footer>";

/** PRINT PAGE NAVIGATOR */
$resultPage = $mysqli->query($queryPage);
$totalPost = ($resultPage->fetch_assoc())['num'];
$totalPages = ceil($totalPost/$numPost);

if ($totalPages > 1) { // If there are multiple pages
    echo "<div id='pageNavigator'>"; // Start navigatorf
    
    $pageEnd = $totalPages;
    $pageStart = 1;

    /** DEFINE RANGE OF PAGES TO BE DISPLAYED */
    if ($totalPages > $numPage) { // If there are more pages than number of pages to display
        if ($page < ($numPage / 2)) $pageEnd = $numPage;
        else if ($page >= $totalPages - ($numPage / 2)) $pageStart = $totalPages - $numPage + 1;
        else {
            $pageStart = $page - floor($numPage / 2);
            $pageEnd = $page + floor($numPage / 2);
        }
    }

    /** PRINT PAGES */
    if ($page > 2) echo "<a href='index.php?page=".($page-1)."'>&#9664; prev</a>&nbsp;|&nbsp;";
    elseif ($page > 1) echo "<a href='index.php'>&#9664; prev</a>&nbsp;|&nbsp;";

    for($i=$pageStart; $i<=$pageEnd; $i++) {

        if($i==$page) { // Current page
            echo "<strong>".$i."</strong>";
        } else {
            echo"<a href='index.php?page=".$i."'>".$i."</a>";
        }
    }

    if ($page < $totalPages) echo "&nbsp;|&nbsp;<a href='index.php?page=".($page+1)."'>next &#9654;</a>";

    echo "</div>"; // End navigator
}



/** MESSAGE PRINT */
if(isset($_SESSION["msg"])) {
    echo "<div id='msg'>".$_SESSION["msg"]."<div id='closeMsg'>&#9587;</div></div>";
    unset($_SESSION["msg"]);
}

/** END OF PAGE */
include_once "page/footer.html";